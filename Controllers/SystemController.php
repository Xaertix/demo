<?php

/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 17.09.2016
 * Time: 14:59
 */
class SystemController extends BaseController
{
    public function Index()
    {
        $model = array();
        $model['defaultLanguage'] = SettingService::Get('DefaultLanguage');
        $model['languages'] = json_decode(SettingService::Get('LanguagesJson'));
        $model['languageKeys'] = TranslationService::GetAllTranslationKeys();
        $this->View($model);
    }

    public function GetCpvCodes(){

        $languages = json_decode(SettingService::Get('LanguagesJson'));
        $cpvRepository = Database::$entityManager->getRepository("CpvCode");
        $query = $cpvRepository->createQueryBuilder('p')
            ->setMaxResults(10)
            ->setFirstResult(0)
            ->getQuery();

        $results = array();
        $cpvCodes = $query->getResult();
        foreach($cpvCodes as $cpvCode){
            $res = new stdClass();
            $res->id = $cpvCode->getId();
            $res->code = $cpvCode->getCode();
            foreach($languages as $lang){
                $langKey = "lang-" . $lang->abbrevation;
                $res->$langKey = CpvService::GetLanguage($cpvCode->getId(),$lang->abbrevation);
            }
            array_push($results,$res);
        }

        $result = new stdClass();
        $result->current = 1;
        $result->rowsCount = 10;
        $result->rows = $results;
        $result->total = 2;
        echo json_encode($result);
    }

    /**
     * Save global settings in setting view
     */
    public function SaveSettings($post)
    {
        if ($post != null) {
            SettingService::SetKey('DefaultLanguage', $post->defaultLanguage);
            MessengerUtil::Success("System settings succesfully saved!");
        }
        // show index page after save
        $this->Index();
    }

    /**
     * Set system languages from global settings
     */
    public function SetLanguages($post)
    {
        if ($post != null && gettype($post->languages) == 'array') {
            if (count($post->languages) > 0) {
                SettingService::SetKey('LanguagesJson', json_encode($post->languages));
                MessengerUtil::Success("Languages saved succesfully!");
            }
        }
    }

    /**
     * Update language value for specified language
     * Translations tab in global
     */
    public function UpdateLanguageKey($post)
    {
        if ($post != null) {
            TranslationService::UpdateLanguage($post->key, $post->lang, $post->value);
        }
    }

    /*
     * Set active language
     */
    public function SetLanguage($post)
    {
        if ($post != null) {
            TranslationService::SetActiveLanguage($post->language);
        }
    }

    public function SaveCpvCode($post){
        if($post != null){
            $id = CpvService::SaveCpvCode($post->code);
            foreach($post->languages as $lang){
                CpvService::SaveCpvLanguage($id,$lang['lang'],$lang['value']);
            }
            echo json_encode(true);
        }
    }

    /**
     * Messages are rendered through this action
     */
    public function Messages()
    {
        $messages = MessengerUtil::GetMessages();
        $this->View(array("messages" => $messages));
    }


}