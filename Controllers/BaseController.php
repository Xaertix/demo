<?php
class BaseController
{

    protected static $isAuthenticated = false;
    protected static $currentUser = null;

    /*
     * Contructor where auth ticket is retrived if authenticated.
     */
    public function __construct()
    {
        if(self::$currentUser == null){
            if (isset($_SESSION['userId']) && $_SESSION['userId'] != null && $_SESSION['userId'] != '') {

                $user = UserService::Get($_SESSION['userId']);
                if ($user != NULL) {
                    self::$isAuthenticated = true;
                    self::$currentUser = $user;
                }
            }
        }
    }

    /**
     * sets the authenticated user id to session
     * @param $userId
     */
    protected function SetAuth($userId)
    {
        $_SESSION['userId'] = $userId;
    }

    protected function View($model = null)
    {
        if($model == null){
            $model = array();
        }
        $callingAction = debug_backtrace()[1]['function'];
        $callingController = str_replace('Controller', '', get_called_class());
        echo TwigLayer::Render($callingController . '/' . $callingAction . '.tpl', $model);
    }
}