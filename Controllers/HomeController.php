<?php

class HomeController extends BaseController {

    /**
     *
     */
    public function Index(){
        $this->View();
    }

    public function Menu(){

        $model = array();
        if(self::$isAuthenticated == true){
            $model['isAuth'] = true;
            $model['fullName'] = self::$currentUser->getFirstName() . ' ' . self::$currentUser->getLastName();
            $model['role'] = self::$currentUser->getUserRights();
        }
        $model['currentLanguage'] = TranslationService::GetActiveLanguage();
        $model['languages'] = json_decode(SettingService::Get('LanguagesJson'));
        $this->View($model);
    }
}
