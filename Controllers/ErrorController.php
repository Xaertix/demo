<?php
/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 02.10.2016
 * Time: 14:13
 */

class ErrorController extends BaseController {

    public function Index($errorMessage){
        $this->View(array('msg' => $errorMessage));
    }
}