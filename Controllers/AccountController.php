<?php

class AccountController extends BaseController
{

    public function Login($post)
    {
        $validator = (new Validation())
            ->IsRequired("email", "Field E-mail is required!")
            ->IsEmail("email", "E-mail format is not valid!")
            ->IsRequired("password", "Field password is required!");

        // POST REQUEST
        if ($post != null && $validator->Validate($post)) {
            // check if user exists and credidentials match
            $user = UserService::Authenticate($post->email, $post->password);
            // set auth ticket
            if ($user != NULL) {
                $this->SetAuth($user->getId());
                RouteHandler::RedirectToAction("Index", "Home");
            } else {
                MessengerUtil::Error("Username or password is invalid");
            }
        }

        // GET REQUEST
        $this->View();

    }

    public function Logout()
    {
        self::$isAuthenticated = false;
        self::$currentUser = null;
        unset($_SESSION['userId']);

        RouteHandler::RedirectToAction('Index', 'Home');
    }

    public function RegisterAccount($post)
    {

        // post request
        if ($post != null) {
            $validator = (new Validation())
                ->IsRequired("fname", "Field Firstname is required!")
                ->IsRequired("sname", "Field Surname is required!")
                ->IsRequired("email", "Field E-mail is required!")
                ->IsEmail("email", "E-mail format is not valid!")
                ->IsRequired("password", "Field password is required!")
                ->IsTrue("password", $post->password == $post->passwordConfirm, "Password and confirm password mismatch!");

            if ($validator->Validate($post)) {
                $user = new User();
                $user->setFirstName($post->fname);
                $user->setLastName($post->sname);
                $user->setEmail($post->email);
                $user->setPassword($post->password);
                $user->setUserRights('0');
                UserService::Register($user);

                // set auth for user taht he will be loged in after registration
                $this->SetAuth($user->getId());
                RouteHandler::RedirectToAction('Index', 'Home');
            }
        }

        $this->View();
    }

    /**
     * function get users
     * @param $post
     */
    public function QueryUsers($post)
    {
        $userRepo = Database::$entityManager->getRepository('User');
        $result = array();
        if ($post != null) {
            if ($post->type == 'id') {
                $user = UserService::Get($post->value);
                if($user != null){
                    $dto = new UserDto();
                    $dto->id = $user->getId();
                    $dto->firstname = $user->getFirstName();
                    $dto->surname = $user->getLastName();
                    $dto->email = $user->getEmail();
                    $dto->usertype = "not implemented";
                    array_push($result, $dto);
                }
            }
           else {

               $query = $userRepo->createQueryBuilder('p')
                   ->where('p.'.$post->type .' LIKE :word')
                   ->setParameter('word', '%' . $post->value . '%')
                   ->getQuery();
               $users = $query->getResult();
                foreach($users as $user){
                    $dto = new UserDto();
                    $dto->id = $user->getId();
                    $dto->firstname = $user->getFirstName();
                    $dto->surname = $user->getLastName();
                    $dto->email = $user->getEmail();
                    $dto->usertype = "not implemented";
                    array_push($result,$dto);
                }
            }
        }

        echo json_encode($result);
    }

    /**
     * Function for editing user
     * @param $post
     */
    public function EditUser($post)
    {
        $userId = RouteHandler::$request->query->get("id");
        var_dump($_GET);
        $user = UserService::Get($userId);


        $model = array();
        if($user != null){

            if($post != null){
                $validator = (new Validation())
                    ->IsRequired("firstname")
                    ->IsRequired("surname");

                if($validator->Validate($post)){
                    $user->setFirstName($post->firstname);
                    $user->setLastName($post->surname);
                    $userRights = 0;
                    $rights = get_class_vars('UserRights');
                    foreach($rights as $key => $value){
                        if(property_exists($post,$key)){
                            $userRights = $userRights + $value;
                        }
                    }
                    $user->setUserRights($userRights);
                    Database::SaveOrUpdate($user);
                }
            }

            $model['firstname'] = $user->getFirstName();
            $model['id'] = $user->getId();
            $model['surname'] = $user->getLastName();
            $model['email'] = $user->getEmail();
            $model['userrights'] = $user->getUserRights();
            $model['allRights'] = get_class_vars('UserRights');
        }

        $this->View($model);
    }

    /**
     * public function for logged in user to change his/her information
     * @param $post
     */
    public function MyAccount($post)
    {

        $validator = (new Validation())
            ->IsRequired("lang", "Language value cannot be empty!")
            ->IsRequired("fname", "Field First name is required!")
            ->IsRequired('sname', "Field Surname is required!");

        // save called
        $model = array();
        $model['languages'] = json_decode(SettingService::Get('LanguagesJson'));
        if ($post != null && $validator->Validate($post)) {
            self::$currentUser->setSystemLanguage($post->lang);
            self::$currentUser->setFirstName($post->fname);
            self::$currentUser->setLastName($post->sname);
            Database::SaveOrUpdate(self::$currentUser);
            MessengerUtil::Success("User information succesfully saved!");
        }
        $model['userLanguage'] = self::$currentUser->getSystemLanguage();
        $model['email'] = self::$currentUser->getEmail();
        $model['fname'] = self::$currentUser->getFirstName();
        $model['sname'] = self::$currentUser->getLastName();
        $this->View($model);
    }

}

