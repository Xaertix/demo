(function (ep) {

    function SettingsSectionHandler() {

        $(document).ready(function(){
            $("#cpv-table").bootgrid({
                ajax:true,
                url: ep.baseUrl + "index.php?controller=system&action=GetCpvCodes",
                formatters: {
                    "link": function(column, row)
                    {
                        return "<a href='" + ep.baseUrl + "index.php?controller=system&action=EditCpvCode&id=" + row.id + "'>" + $("#cpv-table").attr("edit-title") + "</a>";
                    }
                }
            });
        })


        this.addNewLanguage = function () {
            $(".languages").append('<div class="form-group"><label class="control-label col-sm-1">Language</label><div class="col-sm-5"><input type="text" class="form-control lang"></div><label class="control-label col-sm-1">Abbreviation</label><div class="col-sm-5"><input type="text" class="form-control abbv"></div></div>')

        }

        this.addCpvCode = function($successmsg){
            var code = $('#cpv-code').val();
            var languages = [];
            var $languages = $(".cpv-lang");

            for(var i = 0; i< $languages.length; i++){
                var $lang = $($languages[i]);
                languages.push({
                    lang:$lang.attr('lang'),
                    value: $lang.val()
                });

            }
            var model = {
                code: code,
                languages: languages
            }

            $.post(ep.baseUrl + "index.php?controller=System&action=SaveCpvCode",model,function(result){
                if(result == "true"){
                    $('#cpv-code').val("");
                    $(".cpv-lang").val("");
                    console.log($this);
                    toastr.success($successmsg);
                }

            })
        }

        this.queryUsers = function(){
            var findBy = $("#findByValue").val();
            var findByType = $("#findByType").val();
            var usersTable = $("#usersTable tbody");
            var btnText = $("#usersTable thead th:last-child").attr("btn-name");
            $.post(ep.baseUrl + "index.php?controller=Account&action=QueryUsers",{type:findByType,value:findBy},function(result){
                usersTable.empty();
                var res = JSON.parse(result);
                if(res.length > 0){
                    for(var i= 0; i< res.length; i++){
                        usersTable.append("<tr>" +
                            "<td>" + res[i].email + "</td>" +
                            "<td>" + res[i].firstname + "</td>" +
                            "<td>" + res[i].surname + "</td>" +
                            "<td>" + res[i].usertype + "</td>" +
                            "<td><button class='btn btn-primary edit-user' userId='" + res[i].id + "'>" + btnText +"</button></td>" +
                            "</tr>")
                    }

                }
            });
        }

        $(document).on('click','.edit-user',function(){
            window.location.href = ep.baseUrl + "index.php?controller=account&action=edituser&id=" + $(this).attr("userId");
        })

        this.saveLanguages = function () {
            var $rows = $(".languages .form-group");
            var data = [];

            for (var i = 0; i < $rows.length; i++) {
                data.push({
                    language: $($rows[i]).find(".lang").val().toUpperCase(),
                    abbrevation: $($rows[i]).find(".abbv").val().toUpperCase()
                })
            }

            $.post(ep.baseUrl + "index.php?controller=System&action=SetLanguages", {languages: data}, function (result) {
                location.reload();
            });

        }

        $(document).on('click', '.selected-language a', function (e) {
            e.preventDefault();
            console.log($(this).attr("data-id"));
            $.post("index.php?controller=System&action=SetLanguage", {"language": $(this).attr("data-id")}, function (result) {
                location.reload();
            })
        })

        $(document).on("change", "textarea.language-box", function () {
            var val = $(this).val();
            var key = $(this).attr("data-langkey");
            var abbv = $(this).attr("data-langabbv");
            $.post("index.php?controller=System&action=UpdateLanguageKey", {
                key: key,
                lang: abbv,
                value: val
            }, function (result) {
                console.log(result);
            });
        })
    };

    ep.SettingsHandler = new SettingsSectionHandler();

})(ep);