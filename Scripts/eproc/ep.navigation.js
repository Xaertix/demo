/**
 * Created by Treax on 17.09.2016.
 */

(function(ep){
    function NavigationHandler(){

        $(document).on('click','.nav-tabs li a',function(e){
            e.preventDefault();
            $this = $(this);
            $(".nav-tabs li").removeClass("active");
            $this.parent().addClass("active");
            var target = $this.attr("target");
            $(".ep-tab").removeClass("active");
            $(".ep-tab[tab-id="+ target +"]").addClass("active");
        })
    }

    ep.NavigationHandler = new NavigationHandler();
})(ep);