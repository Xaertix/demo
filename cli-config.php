<?php
/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 13.09.2016
 * Time: 18:37
 */

require_once 'Config/Settings.inc.php';
require_once 'AppStart/Database.php';

/**
 * Mark
 * Was added to circumvent error:
 *
 * PHP Fatal error:  Class 'Entity' not found in /var/www/html/procurement/Core/Entity/Translation.php on line 14
 *
 * ... when running vendor/bin/doctrine orm:schema-tool:drop --force from the command line
 */
require_once 'Core/Entity/Entity.php';
/* Was added to circumvent error end */

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet(Database::$entityManager);