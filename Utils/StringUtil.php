<?php
/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 13.09.2016
 * Time: 22:23
 */

/**
 * Class StringUtil
 */
abstract class StringUtil{

    /**
     * @param $pw
     * @return string
     */
    public static function Hash($pw){
        return sha1($pw);
    }

    public static function MD5($pw){
        return md5($pw);
    }
}