<?php

/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 19.09.2016
 * Time: 0:01
 */
class UrlUtil
{
    public function Action($action,$controller){
        if(_HTACCESS_INITIALIZED_){
            return RouteHandler::$request->getBasePath() . '/' .$controller . '/' . $action;
        }
        else{
            return RouteHandler::$request->getBasePath() . '/index.php?action=' . $action . '&controller=' . $controller;
        }

    }

    public function Content($url){
        return RouteHandler::$request->getBasePath() . "$url";
    }
}