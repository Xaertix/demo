<?php

/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 02.10.2016
 * Time: 21:36
 */
class Validation
{

    private $validationModel = array();

    private function SetModel($prop, $model)
    {
        if (!array_key_exists($prop, $this->validationModel)) {
            $this->validationModel[$prop] = array();
        }
        array_push($this->validationModel[$prop], $model);
    }


    public function IsRequired($prop, $errorMessage = null)
    {

        $model = new stdClass();
        $model->isValid = function ($val) {
            if ($val == "") {
                return false;
            } else {
                return true;
            }
        };
        $model->errorMessage = $errorMessage;
        $this->SetModel($prop, $model);
        return $this;
    }

    public function IsEmail($prop, $errorMessage = null)
    {
        $model = new stdClass();
        $model->isValid = function ($val) {
            return filter_var($val, FILTER_VALIDATE_EMAIL);
        };
        $model->errorMessage = $errorMessage;
        $this->SetModel($prop, $model);
        return $this;
    }

    public function IsTrue($prop, $validationFunction, $errorMessage)
    {
        $model = new stdClass();
        $model->isValid = $validationFunction;
        $model->errorMessage = $errorMessage;
        $this->SetModel($prop, $model);
        return $this;
    }

    public function Validate($model)
    {
        $isValid = true;
        foreach ($this->validationModel as $key => $validators) {
            if (property_exists($model, $key)) {
                foreach ($validators as $validator) {
                    if (is_bool($validator->isValid)) {
                        if ($validator->isValid == false) {
                            $isValid = false;
                        }
                    } else {
                        $validationFunction = $validator->isValid;
                        if (!$validationFunction($model->$key)) {
                            $isValid = false;
                        }
                    }

                    if (!$isValid && $validator->errorMessage != null) {
                        MessengerUtil::Error($validator->errorMessage);
                    }
                }
            }
        }
        return $isValid;
    }
}