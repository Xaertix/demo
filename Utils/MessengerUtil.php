<?php
/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 08.10.2016
 * Time: 12:58
 */
require_once 'Core/Enum/MessageTypeEnum.php';

class MessengerUtil{

    public static function GetMessages(){
        if(isset($_SESSION['messages']) && count($_SESSION['messages']) > 0){
            $messages = $_SESSION['messages'];
            $_SESSION['messages'] = array();
            return array_reverse($messages);
        }
        else{
            return array();
        }
    }

    /**
     * Add success message to messages array
     * @param $message
     */
    public static function Success($message){
        $msg = new stdClass();
        $msg->type = MessageType::$Success;
        $msg->msg = TranslationService::Translate($message);
        self::AddMessage($msg);
    }

    /**
     * Add error message to messages array
     * @param $message
     */
    public static function Error($message){
        $msg = new stdClass();
        $msg->type = MessageType::$Error;
        $msg->msg = TranslationService::Translate($message);
        self::AddMessage($msg);
    }

    /**
     * Add warning message to messages array
     * @param $message
     */
    public static function Warning($message){
        $msg = new stdClass();
        $msg->type = MessageType::$Warning;
        $msg->msg = TranslationService::Translate($message);
        self::AddMessage($msg);
    }

    private static function AddMessage($msg){

        if(isset($_SESSION['messages']) && is_array($_SESSION['messages'])){
            $messages = $_SESSION['messages'];
        }
        else{
            $_SESSION['messages'] = array();
            $messages = array();
        }

        array_push($messages , $msg);
        $_SESSION['messages'] = $messages;
    }
}