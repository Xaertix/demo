<?php
/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 17.09.2016
 * Time: 14:44
 */

class UserRights{
    public static $global_settings = 1;
    public static $global_languages = 2;
    public static $global_translations = 4;
    public static $global_cpvcodes = 8;
    public static $global_users = 16;

}