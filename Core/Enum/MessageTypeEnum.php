<?php
/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 02.10.2016
 * Time: 22:36
 */

class MessageType{
    public static $Error = 0;
    public static $Success = 1;
    public static $Warning = 2;
}