<?php
/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 09.10.2016
 * Time: 18:08
 */

class UserDto{
    public $email;
    public $firstname;
    public $surname;
    public $usertype;
    public $id;
}