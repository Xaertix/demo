<?php
/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 16.09.2016
 * Time: 18:20
 */

require_once "Core/Entity/Entity.php";


/**
 * @Entity
 * @Table(name="translations")
 */
class Translation extends Entity {

    /** @Column(type="string", name="`Key`") */
    protected $key;

    /** @Column(type="string", name="`Value`") */
    protected $value;

    /** @Column(type="string", name="`Language`") */
    protected $language;

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }
}