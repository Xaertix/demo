<?php

/** @Entity
 * @Table(name="cpvcodes")
 */
class CpvCode extends Entity {

    /** @Column(type="string", name="Code") */
    protected $code;

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }
}