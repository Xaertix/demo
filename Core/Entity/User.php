<?php

require_once "Core/Entity/Entity.php";

/** @Entity
 * @Table(name="users")
 */
class User extends Entity {



    /** @Column(type="string", name="FirstName") */
    protected $firstName;

    /** @Column(type="string", name="LastName") */
    protected $lastName;

    /** @Column(type="string", name="Password") */
    protected $password;

    /** @Column(type="string", name="Email") */
    protected $email;

    /** @Column(type="string", name="SystemLanguage") */
    protected $systemLanguage;

    /** @Column(type="integer", name="UserRights") */
    protected $userRights;



    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }



    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getUserRights()
    {
        return $this->userRights;
    }

    /**
     * @param mixed $userRights
     */
    public function setUserRights($userRights)
    {
        $this->userRights = $userRights;
    }

    /**
     * @return mixed
     */
    public function getSystemLanguage()
    {
        return $this->systemLanguage;
    }

    /**
     * @param mixed $systemLanguage
     */
    public function setSystemLanguage($systemLanguage)
    {
        $this->systemLanguage = $systemLanguage;
    }
}