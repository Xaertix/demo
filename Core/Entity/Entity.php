<?php
/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 13.09.2016
 * Time: 17:52
 */

abstract class Entity {
    /** @Id @Column(type="integer", name="Id") @GeneratedValue */
    protected $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

}