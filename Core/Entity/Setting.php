<?php

require_once "Core/Entity/Entity.php";

/**
 * @Entity
 * @Table(name="settings")
 */
class Setting extends Entity {

    /** @Column(type="string", name="`Key`") */
    protected $key;

    /** @Column(type="string", name="Value") */
    protected $value;

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }
}
