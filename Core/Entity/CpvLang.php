<?php

/**
 * @Entity
 * @Table(name="cpvlanguages")
 */
class CpvLang extends Entity{

    /** @Column(type="integer", name="CpvCodeId") */
    protected $cpvCodeId;

    /** @Column(type="string", name="LanguageKey") */
    protected $language;

    /** @Column(type="string", name="Title") */
    protected $title;

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }


    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param mixed $cpvCodeId
     */
    public function setCpvCodeId($cpvCodeId)
    {
        $this->cpvCodeId = $cpvCodeId;
    }

    /**
     * @return mixed
     */
    public function getCpvCodeId()
    {
        return $this->cpvCodeId;
    }
}