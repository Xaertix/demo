<div class="container padding-top-10">
    <div class="row">
        <div class="col-md-4 col-lg-4 col-sm-4"></div>
        <div class="col-md-4 col-lg-4 col-sm-4 panel panel-default padding-all-10">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <form method="post" action="{{URL.Action("login","account")}}">
                        <div class="form-group">
                            <label for="email">{{__("E-mail")}}</label>
                            <input id="email" type="text" name="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="password">{{__("Password")}}</label>
                            <input id="password" type="password" name="password" class="form-control">
                        </div>

                        <button class="btn btn-primary float-right" type="submit">{{__("Log In")}}</button>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    {{__("Have you")}} <a href="{{URL.Action("ForgotPassword","Account")}}">{{__("forgotten your password?")}}</a>
                </div>
                <div class="col-lg-12 col-sm-12 col-md-12">
                    {{__("No account yet?")}} <a href="{{URL.Action("RegisterAccount","Account")}}">{{__("Register")}}</a>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-lg-4 col-sm-4"></div>
    </div>
</div>
