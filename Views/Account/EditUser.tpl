<div class="container padding-top-10">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h3>{{__("Edit user")}}</h3>
        </div>
    </div>
    <div class="row">
        <form method="post" action="{{URL.Action("EditUser","Account")}}&id={{id}}">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="email">{{__("E-mail")}}</label>
                    <input class="form-control" disabled type="text" id="email" name="email" value="{{email}}">
                </div>
                <div class="form-group">
                    <label for="firstname">{{__("First name")}}</label>
                    <input class="form-control" type="text" id="firstname" name="firstname" value="{{firstname}}">
                </div>
                <div class="form-group">
                    <label for="surname">{{__("Surname")}}</label>
                    <input class="form-control" type="text" id="surname" name="surname" value="{{surname}}">
                </div>

            </div>
            <div class="col-lg-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <label>{{__("User rights")}}</label>

                </div>
                <div class="form-group">
                    {% for key,value in allRights %}
                    {%if(userrights b-and value)%}
                        <label class="checkbox-inline"><input type="checkbox" name="{{key}}" checked>{{__(key)}}</label>
                    {%else%}
                        <label class="checkbox-inline"><input type="checkbox" name="{{key}}">{{__(key)}}</label>
                    {%endif%}
                    {% endfor %}
                </div>
            </div>
            <div class="col-lg-12 col-sm-12 col-md-12">
                <button class="btn btn-primary float-right">{{__("Save")}}</button>
            </div>
        </form>
    </div>
</div>