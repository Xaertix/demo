<div class="container padding-top-10">

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <h3>{{__("My account")}}</h3>
        </div>
    </div>

    <div class="row">
        <form method="post" action="{{URL.Action("MyAccount","Account")}}">
        <div class="col-lg-12 col-sm-12 col-md-12">
            <div class="form-group">
                <label for="email">{{__("E-mail")}}</label>
                <input id="email" type="text" name="email" class="form-control" disabled value="{{email}}">
            </div>
            <div class="form-group">
                <label for="fname">{{__("First name")}}</label>
                <input id="fname" type="text" name="fname" class="form-control" value="{{fname}}">
            </div>
            <div class="form-group">
                <label for="sname">{{__("Surname")}}</label>
                <input id="sname" type="text" name="sname" class="form-control" value="{{sname}}">
            </div>
            <div class="form-group">
                <label for="lang">{{__("System language")}}</label>
                <select id="lang" name="lang" class="form-control">
                    {%for lang in languages%}
                        <option value='{{lang.abbrevation}}' {% if(userLanguage == lang.abbrevation) %} selected='selected' {% endif %}>{{lang.language}}</option>
                    {% endfor%}
                </select>
            </div>

            <div class="form-group padding-top-10">
                <label for="password">{{__("Password")}}</label>
                <input id="password" type="password" name="password" class="form-control">
            </div>
            <div class="form-group">
                <label for="newpw">{{__("New Password")}}</label>
                <input id="newpw" type="password" name="newpw" class="form-control">
            </div>
            <div class="form-group">
                <label for="newpwconfirm">{{__("Confirm Password")}}</label>
                <input id="newpwconfirm" type="password" name="newpwconfirm" class="form-control">
            </div>
        </div>
        <div class="col-lg-12 col-sm-12 col-md-12">
            <button class="btn btn-primary float-right">Save</button>
        </div>
        </form>
    </div>
</div>