
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Title</title>

    <link type="text/css" href="{{URL.Content("/Content/bootstrap.min.css")}}" rel="stylesheet"/>
    <link type="text/css" href="{{URL.Content("/Content/jquery.bootgrid.min.css")}}" rel="stylesheet"/>
    <link type="text/css" href="{{URL.Content("/Content/toastr.min.css")}}" rel="stylesheet"/>
    <link type="text/css" href="{{URL.Content("/Content/Site.css")}}" rel="stylesheet"/>


</head>
<body>
<!-- Render Menu -->
{{RenderPartial('Menu','Home')}}
<!-- Render body -->
{{RenderPartial(action,controller)}}
<!--
Scripts should be called allways after this comment.
-->
<script type="text/javascript" src="{{URL.Content("/Scripts/jquery/jquery-3.1.0.min.js")}}"></script>
<script type="text/javascript" src="{{URL.Content("/Scripts/grid/jquery.bootgrid.min.js")}}"></script>
<script type="text/javascript" src="{{URL.Content("/Scripts/grid/jquery.bootgrid.fa.min.js")}}"></script>
<script type="text/javascript" src="{{URL.Content("/Scripts/toastr/toastr.min.js")}}"></script>
<script type="text/javascript" src="{{URL.Content("/Scripts/core/core.js")}}"></script>
<script type="text/javascript" src="{{URL.Content("/Scripts/bootstrap/bootstrap.min.js")}}"></script>
<script type="text/javascript" src="{{URL.Content("/Scripts/eproc/ep.navigation.js")}}"></script>
<script type="text/javascript" src="{{URL.Content("/Scripts/eproc/ep.settings.js")}}"></script>

<script type="text/javascript">
    ep.baseUrl = "{{URL.Content("/")}}";
</script>

{{RenderPartial("Messages","System")}}

</body>
</html>
