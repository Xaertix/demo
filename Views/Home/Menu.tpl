<?php
/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 08.09.2016
 * Time: 20:28
 */
?>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li ><a href="{{URL.Action("index","home")}}">Home</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{currentLanguage}}<span class="caret"></span></a>
                    <ul class="dropdown-menu selected-language">
                        {% for lang in languages %}
                        <li><a href="#" data-id="{{lang.abbrevation}}">{{lang.language}}</a></li>
                        {% endfor %}
                    </ul>
                </li>
            </ul>

            {% if isAuth == true %}
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{fullName}}<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{URL.Action("myaccount","account")}}">{{__("My Account")}}</a></li>
                        {% if(HasRights(1) or HasRights(4) or HasRights(8) or HasRights(16))%}
                        <li><a href="{{URL.Action("index","system")}}">{{__("System settings")}}</a></li>
                        {% endif %}
                        <li><a href="{{URL.Action("logout","account")}}">{{__("Log out")}}</a></li>
                    </ul>
                </li>
            </ul>
            {% else %}
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{URL.Action("login","account")}}">{{__("Log In")}}</a></li>
            </ul>
            {% endif %}

    </div>
    </div>
</nav>