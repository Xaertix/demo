<div class="container">
    <ul class="nav nav-tabs">
        {% if(HasRights(1) or HasRights(2)) %}
        <li class="active"><a href="#" target="settings">{{__("Settings")}}</a></li>{% endif %}
        {% if(HasRights(4)) %}
        <li><a href="#" target="translations">{{__("Translations")}}</a></li>{% endif %}
        {% if(HasRights(8)) %}
        <li><a href="#" target="cpvcodes">{{__("CPV Codes")}}</a></li>{% endif %}
        {% if(HasRights(16)) %}
        <li><a href="#" target="users">{{__("Users")}}</a></li>{% endif %}
    </ul>
</div>

<div class="container">
    <div class="row ep-tab active" tab-id="settings">
        {% if(HasRights(2)) %}
        <h4>{{__("Languages")}}</h4>
        <div class="col-lg-12 col-sm-12 col-md-12">
            <div class="form-horizontal languages">
                {% for lang in languages %}
                <div class="form-group">

                    <label class="control-label col-sm-1">Language</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control lang" value="{{lang.language}}">
                    </div>
                    <label class="control-label col-sm-1">Abbreviation</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control abbv" value="{{lang.abbrevation}}">
                    </div>

                </div>
                {% endfor %}
            </div>

            <div class="form-group float-right">
                <button class="btn btn-info"
                        onclick="ep.SettingsHandler.addNewLanguage()">{{__("Add Language")}}</button>
                <button class="btn btn-primary" onclick="ep.SettingsHandler.saveLanguages()">{{__("Save")}}</button>
            </div>
        </div>{%endif%}
        {% if(HasRights(1)) %}
        <h4>{{__("Global settings")}}</h4>
        <form method="post" action="{{URL.Action("SaveSettings","System")}}">
            <div class="col-lg-12 col-sm-12 col-md-12">

                <div class="form-group">
                    <label for="defaultLanguage">{{__("System default language")}}</label>
                    <select id="defaultLanguage" name="defaultLanguage" class="form-control">
                        {% for lang in languages %}
                        <option value='{{lang.abbrevation}}' {% if(defaultLanguage == lang.abbrevation) %}
                                selected='selected' {% endif %}>{{lang.language}}</option>
                        {% endfor %}
                    </select>
                </div>

                <div class="form-group float-right">
                    <button class="btn btn-primary">{{__("Save")}}</button>
                </div>
        </form>
    </div>

    {%endif%}
</div>
{% if(HasRights(4)) %}
<div class="row ep-tab" tab-id="translations">
    <h4>{{__("Translations")}}</h4>
    <div class="col-md-12 col-lg-12 col-sm-12">
        <table class="table table-responsive">
            <thead>
            <th>{{__("Translation Key")}}</th>
            {% for lang in languages %}
            <th>{{lang.abbrevation}}</th>
            {% endfor %}
            </thead>
            <tbody>
            {% for langKey in languageKeys %}
            <tr>
                <td>{{langKey.value}}</td>
                {% for lang in languages %}
                <td><textarea class="language-box" data-langKey="{{langKey.key}}"
                              data-langAbbv="{{lang.abbrevation}}">{{__(langKey.key,true,lang.abbrevation)}}</textarea>
                </td>
                {% endfor %}
            </tr>
            {% endfor %}
            </tbody>
        </table>
    </div>
</div>{% endif %}
{% if(HasRights(8)) %}
<div class="row ep-tab padding-top-10" tab-id="cpvcodes">
    <div class="col-lg-12 col-sm-12 col-md-12 panel panel-default padding-all-10 form-inline ">
        <div class="form-group">
            <label>{{__("CPV Code")}}</label>
            <input id="cpv-code" type="text" class="form-control">
        </div>
        {% for lang in languages%}
        <div class="form-group">
            <label>{{lang.abbrevation}}</label>
            <input type="text" lang="{{lang.abbrevation}}" class="form-control cpv-lang">
        </div>
        {% endfor %}
        <div class="form-group">
            <button onclick="ep.SettingsHandler.addCpvCode('{{__("CPV code added succesfully!")}}')" class="btn btn-primary">{{__("Add")}}</button>
        </div>
    </div>
    <div class="col-lg-12 col-sm-12 col-md-12 padding-top-10">
        <table id="cpv-table" edit-title="{{__("Edit")}}" class="table table-condensed table-hover table-striped">
            <thead>
            <tr>
                <th data-column-id="code">{{__("Code")}}</th>
                {% for lang in languages%}
                <th data-column-id="lang-{{lang.abbrevation}}">{{lang.abbrevation}}</th>
                {% endfor %}
                <th data-column-id="link" data-formatter="link" data-sortable="false">{{__("Edit")}}</th>
            </tr>
            </thead>
        </table>
    </div>
</div>{% endif %}
{% if(HasRights(16)) %}
<div class="row ep-tab" tab-id="users">

    <div class="col-lg-12 col-sm-12 col-md-12 form-inline padding-top-10">

        <div class="form-group">
            <label for="findByType">Find user by</label>
            <select class="form-control" id="findByType">
                <option value="id">{{__("Identification nr")}}</option>
                <option value="email">{{__("Email")}}</option>
                <option value="firstName">{{__("First name")}}</option>
                <option value="lastName">{{__("Surname")}}</option>
            </select>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" id="findByValue" placeholder="{{__("Expression")}}">
        </div>
        <button id="findUser" type="submit" class="btn btn-primary"
                onclick="ep.SettingsHandler.queryUsers()">{{__("Find")}}</button>

    </div>
    <div class="col-lg-12 col-sm-12 col-md-12 padding-top-10">
        <table id="usersTable" class="table table-bordered">
            <thead>
            <tr>
                <th>{{__("E-mail")}}</th>
                <th>{{__("First name")}}</th>
                <th>{{__("Surname")}}</th>
                <th>{{__("Type")}}</th>
                <th btn-name="{{__("Edit user")}}"></th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

</div>{% endif %}

</div>

