<script type="text/javascript">
    {% for message in messages %}
    {% if(message.type == 0) %}
    toastr.error('{{message.msg}}');

    {% endif %}
    {% if(message.type == 1) %}
    toastr.success('{{message.msg}}');
    {% endif %}
    {% if(message.type == 2) %}
    toastr.warning('{{message.msg}}');
    {% endif %}

    {% endfor%}
</script>