<div class="container" style="margin-top:10px; margin-bottom:10px">
    <div class="row alert alert-danger">
        <div class="col-lg-12 col-sm-12 col-md-12">
            <h3 class="">{{__("SYSTEM ERROR")}}</h3>
            <div style="color:#000">{{__("Exception message")}}: {{msg}}</div>
        </div>
    </div>
</div>
