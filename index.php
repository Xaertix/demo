<?php


session_start();
session_regenerate_id();

/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 05.09.2016
 * Time: 21:38
 */

/*
* This is the main file that is excecuted in every call and starts the workflow
* controller is the handler and action is the view of the handler
* default is home index
*/

require_once 'vendor/autoload.php';

require_once 'Config/Config.php';
require_once 'Utils/Utils.php';
require_once 'Services/BaseService.php';
require_once 'AppStart/RouteHandler.php';

RouteHandler::Start();
