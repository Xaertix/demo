<?php

require_once 'Core/Entity/CpvCode.php';
require_once 'Core/Entity/CpvLang.php';

class CpvService extends BaseService {

    private static function CpvRepository() {
        return Database::$entityManager->getRepository('CpvCode');
    }

    private static function CpvLangRepository() {
        return Database::$entityManager->getRepository('CpvLang');
    }

    public static function SaveCpvCode($code){
        $cpv = new CpvCode();
        $cpv->setCode($code);
        Database::SaveOrUpdate($cpv);
        return $cpv->getId();
    }

    public static function GetLanguage($id,$langKey){
        $lang = self::CpvLangRepository()->findOneBy(array("cpvCodeId" => $id, "language" => $langKey));
        if($lang ==  null){
            return "";
        }
        else{
            return $lang->getTitle();
        }
    }

    public static function SaveCpvLanguage($id,$language,$value){
        $lang = new CpvLang();
        $lang->setCpvCodeId($id);
        $lang->setLanguage($language);
        $lang->setTitle($value);
        Database::SaveOrUpdate($lang);
    }
}