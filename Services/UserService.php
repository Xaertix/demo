<?php
/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 13.09.2016
 * Time: 22:32
 */

require_once 'Core/Entity/User.php';
require_once 'Core/Enum/UserRghts.php';
require_once 'Core/DTO/UserDto.php';

class UserService extends BaseService
{

    private static function UserRepository()
    {
        return Database::$entityManager->getRepository('User');
    }


    /**
     * @param $id
     * @return
     * @internal param $id $return user object* $return user object
     */
    public static function Get($id)
    {
        return self::UserRepository()->findOneBy(array('id' => $id));
    }

    public static function Register($user)
    {
        $user->setSystemLanguage(TranslationService::GetActiveLanguage());
        Database::SaveOrUpdate($user);
    }

    /**
     * @param $user
     * @param $pw
     * @return mixed
     */
    public static function Authenticate($email, $pw)
    {
        return self::UserRepository()->findOneBy(array('email' => $email, 'password' => StringUtil::Hash($pw)));
    }

    /**
     * @param $user
     * @return bool
     */
    public static function Exists($email)
    {
        return (self::UserRepository()->findOneBy(array('email' => $email)) != NULL);
    }

    /*
     * Bitwise operator to check rights
     */
    public static function UserHasRights($right, $userId = null)
    {
        $user = null;
        if ($userId != null) {
            $user = self::Get($userId);
        } else {
            if (isset($_SESSION['userId'])) {
                $user = self::Get($_SESSION['userId']);
            }
        }
        if ($user != null) {
            if(($user->getUserRights() & $right) == $right){
                return true;
            }
        }
        return false;
    }


}
