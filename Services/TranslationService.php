<?php
/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 16.09.2016
 * Time: 18:19
 */

require_once 'Core/Entity/Translation.php';

class TranslationService extends BaseService {

    private static function TranslationRepository(){
        return Database::$entityManager->getRepository('Translation');
    }

    public static function GetAllTranslationKeys(){
        $all = self::TranslationRepository()->findBy(array('language' => _SYSTEM_LANGUAGE_ABBV_));
        return $all;
    }

    public static function UpdateLanguage($key,$lang,$value){
        $translation = self::TranslationRepository()->findOneBy(array('key' => $key, 'language' => $lang));
        if($translation == null){
            $translation = new Translation();
            $translation->setKey($key);
            $translation->setValue($value);
            $translation->setLanguage($lang);
        }
        else{
            $translation->setValue($value);
        }
        Database::SaveOrUpdate($translation);
    }

    // todo add caching
    public static function Translate($key, $isMD5 = false,$language = null){
        $md5Key = StringUtil::MD5($key);

        if($language == null){
            $language = self::GetActiveLanguage();
        }

        if($isMD5){
            $md5Key = $key;
        }
        $translation = self::TranslationRepository()->findOneBy(array('key' => $md5Key, 'language' => $language));
        if($translation != null){
            $value = $translation->getValue();
            if($value == ''){
                return $key;
            }
            return $value;
        }
        if($translation == null && $isMD5){
            return '';
        }

        if($translation == null && self::GetActiveLanguage() == _SYSTEM_LANGUAGE_ABBV_){
            $translation = new Translation();
            $translation->setKey($md5Key);
            $translation->setLanguage(_SYSTEM_LANGUAGE_ABBV_);
            $translation->setValue($key);

            Database::$entityManager->persist($translation);
            Database::$entityManager->flush();
        }

        return $key;
    }


    public static function GetActiveLanguage(){
        $language = SettingService::Get('DefaultLanguage');
        if(isset($_SESSION['lang'])){
            $language = $_SESSION['lang'];
        }
        return $language;
    }

    public static function SetActiveLanguage($lang){
        $_SESSION['lang'] = $lang;
    }
}