<?php
/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 17.09.2016
 * Time: 17:09
 */
require_once 'Core/Entity/Setting.php';

class SettingService extends BaseService {
    private static function SettingsRepository(){
        return Database::$entityManager->getRepository('Setting');
    }

    public static function Get($key){
        $setting = self::SettingsRepository()->findOneBy(array('key' => $key));
        if($setting != null){
            return $setting->getValue();
        }
        return null;
    }

    public static function IsKeySet($key){
        $setting = self::SettingsRepository()->findOneBy(array('key' => $key));
        if($setting != null){
            return true;
        }
        return false;
    }

    /**
     * @param $key
     * @param $value
     */
    public static function SetKey($key, $value){
        $setting = self::SettingsRepository()->findOneBy(array('key' => $key));
        if($setting == null){
            $setting = new Setting();
            $setting->setKey($key);
            $setting->setValue($value);
        }
        else{
            $setting->setValue($value);
        }
        Database::$entityManager->persist($setting);
        Database::$entityManager->flush();

    }
}