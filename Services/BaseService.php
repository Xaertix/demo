<?php
/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 13.09.2016
 * Time: 22:31
 */

abstract class BaseService {
}

require_once 'AppStart/Database.php';

// load all repositories to project
require_once 'Services/UserService.php';
require_once 'Services/TranslationService.php';
require_once 'Services/SettingService.php';
require_once 'Services/CpvService.php';