<?php 

define('_DEFINES_INCLUDED_', '1');

if (!defined('_EP_VERSION_')) {
    define('_EP_VERSION_', '0.0.1');
}

$currentDir = dirname(__FILE__);

/* Directories */
if (!defined('_EP_ROOT_DIR_')) {
    define('_EP_ROOT_DIR_', realpath($currentDir.'/..'));
}

if (!defined('_EP_VENDOR_DIR_')) {
    define('_EP_VENDOR_DIR_', _EP_ROOT_DIR_.'/vendor/');
}

if (!defined('_EP_VIEWS_DIR_')) {
    define('_EP_VIEWS_DIR_', _EP_ROOT_DIR_.'/Views/');
    define('_EP_TWIG_VIEWS_DIR_', _EP_VIEWS_DIR_);
}