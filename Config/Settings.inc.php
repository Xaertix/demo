<?php
/**
 * This file should be removed from git tracking in near future.
 * Everyone (You, Me, Production Server, Dev Server) should have his own
 */
define('_SETTINGS_INLCUDED_', '1');
/**
 * Didn't work for me atm, bc my root path is 192.168.0.18/procurement/
 * so .htacces RewriteRule regex won't eat 192.168.0.18/procurement/handler/action
 * Best idea so far is to make a couple of fields in backend config:
 * 0. Use https (Y/N)
 * 1. Domain (f.eg. 192.168.0.18)
 * 2. Root Path (f.eg procurement/)
 * in the future
 * Then we can use those db var to dynamically generate .htaccess
 */
define('_HTACCESS_INITIALIZED_', 0);

define('_DB_SERVER_', 'localhost');
define('_DB_NAME_', 'eprocurement');
define('_DB_USER_', 'procurement');
define('_DB_PASSWD_', 'ipconfig');
define('_DB_PORT_', '3306');
define('_SYSTEM_LANGUAGE_ABBV_', 'EN');
define('_SYSTEM_LANGUAGE_', 'ENGLISH');

@ini_set('display_errors', 'on');
@error_reporting(E_ALL | E_STRICT);
