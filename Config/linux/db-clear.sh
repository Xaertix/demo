#!/bin/bash



# includes
. sh-utils # includes fs-config

checkFileExecOrExit db-config

. db-config
# includes end



# check db config env
[[ -z ${MYSQL_USER} || -z ${MYSQL_PASSWORD} 
    || -z ${EP_DB_SERVER} || -z ${EP_DB_NAME} 
    || -z ${EP_DB_USER} || -z ${EP_DB_PASSWORD} 
    || -z ${EP_DB_PORT} ]] && {
    log_err 'db env not configured'
}
# check db config env end



# ask this
to_stdout "Do you wish to clear local db?"
read -r -p "Are you sure? [y/N] " response
[[ $response =~ ^([yY][eE][sS]|[yY])$ ]] || { exit 1; }
# ask this end



# mysql interactions
mysql -u ${MYSQL_USER} -p${MYSQL_PASSWORD} << EOF
DROP DATABASE IF EXISTS ${EP_DB_NAME};
DROP USER '${EP_DB_USER}'@'${EP_DB_SERVER}';
EOF

mysql_resp=$?

[[ ${mysql_resp} -ne ${MYSQL_SUCCES_RESP} ]] && {
    log_err "mysql returned ${mysql_resp}"
    exit 4
}
#mysql interactions end



to_stdout "All OK with db drop"
