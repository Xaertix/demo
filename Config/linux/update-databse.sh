#!/bin/bash

. sh-utils

[[ -z ${PROJECT_ROOT} || -z ${VENDOR_ROOT} || -z ${DEFAULT_SUCCESS_RESP} ]] && {
    log_err "fs env not configured"
}

bin_doctrine=${VENDOR_ROOT}/bin/doctrine

checkFileExecOrExit ${bin_doctrine}

currentDir=$(pwd)
cd ${PROJECT_ROOT}

evalOkOrExit "${bin_doctrine} orm:schema-tool:update --force --dump-sql"

cd ${currentDir}
