# Quickly initialize project settings file(s) and create the mysql database

#!/bin/bash



# includes
. sh-utils # includes fs-config

checkFileExecOrExit db-config

. db-config
# includes end



# check db config env
[[ -z ${MYSQL_USER} || -z ${MYSQL_PASSWORD} 
    || -z ${EP_DB_SERVER} || -z ${EP_DB_NAME} 
    || -z ${EP_DB_USER} || -z ${EP_DB_PASSWORD} 
    || -z ${EP_DB_PORT} ]] && {
    log_err 'db env not configured'
    exit 7
}
# check db config env end



# check fs config env
[[ -z ${PROJECT_ROOT} || -z ${MYSQL_SUCCES_RESP} ]] && {
    log_err 'fs env not configured'
    exit 7
}
# check fs config env end



# Settings.inc.php file config
settings_path=${PROJECT_ROOT}/Config/Settings.inc.php
settings_dir=$(dirname "${settings_path}")

[[ -f ${settings_path} ]] && {
    log_err "${settings_path} already exists. Project configured?"
    exit 2;
}

[[ -d ${settings_dir} ]] || {
    log_err "${settings_dir} not found."
    exit 2;
}

## generate content of settings file
read -d '' settings_content << EOF 
<?php
/**
 * This file should be removed from git tracking in near future.
 * Everyone (You, Me, Production Server, Dev Server) should have his own
 */
define('_SETTINGS_INLCUDED_', '1');

define('_DB_SERVER_', '${EP_DB_SERVER}');
define('_DB_NAME_', '${EP_DB_NAME}');
define('_DB_USER_', '${EP_DB_USER}');
define('_DB_PASSWD_', '${EP_DB_PASSWORD}');
define('_DB_PORT_', '${EP_DB_PORT}');
EOF

echo "${settings_content}"  > ${settings_path}
# Settings.inc.php file config end

mysql -u ${MYSQL_USER} -p${MYSQL_PASSWORD} << EOF
CREATE DATABASE ${EP_DB_NAME};
CREATE USER '${EP_DB_USER}'@'${EP_DB_SERVER}' IDENTIFIED BY '${EP_DB_PASSWORD}';
GRANT USAGE ON *.* to '${EP_DB_USER}'@'${EP_DB_SERVER}' IDENTIFIED BY '${EP_DB_PASSWORD}';
GRANT ALL PRIVILEGES ON ${EP_DB_NAME}.* TO '${EP_DB_USER}'@'${EP_DB_SERVER}';
EOF

mysql_resp=$?

[[ ${mysql_resp} -ne ${MYSQL_SUCCES_RESP} ]] && {
    log_err "mysql returned ${mysql_resp}"
    exit 4
}

to_stdout "All OK with db and user create"



[[ -z ${IMPORT_DB} ]] || {
    [[ -z ${IMPORT_SQL_PATH} ]] && {
        log_err 'import file not set'
        exit 3
    }
    log "start import of ${PROJECT_ROOT}${IMPORT_SQL_PATH} to ${EP_DB_NAME}"
    mysql -u ${EP_DB_USER} -p${EP_DB_PASSWORD} ${EP_DB_NAME} < ${PROJECT_ROOT}/${IMPORT_SQL_PATH}
}