<?php
/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 09.09.2016
 * Time: 21:14
 */
if(!defined('_SETTINGS_INLCUDED_')) {
    die(__FILE__ . ': settings not found');
}



use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

class Database{

    public static $entityManager = null;
    private static $isInitialize = false;

    public static function initialize(){
        if(self::$isInitialize){
            return;
        }
        $config = Setup::createAnnotationMetadataConfiguration(array('Core/Entity'), true);
        self::$entityManager = EntityManager::create(array(
            'driver'   => 'pdo_mysql',
            'user'     => _DB_USER_,
            'password' => _DB_PASSWD_,
            'dbname'   => _DB_NAME_,
        ), $config);
    }

    public static function qb(){
        return self::$entityManager->createQueryBuilder();
    }

    public static function SaveOrUpdate($entity){
        self::$entityManager->persist($entity);
        self::$entityManager->flush();
    }
}

// init database
Database::initialize();

