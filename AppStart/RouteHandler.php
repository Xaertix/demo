<?php
/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 05.09.2016
 * Time: 21:38
 */

use Symfony\Component\HttpFoundation\Request;
// use Symfony\Component\Routing\Matcher\UrlMatcher;
// use Symfony\Component\Routing\RequestContext;
// use Symfony\Component\Routing\RouteCollection;
// use Symfony\Component\Routing\Route;


require_once 'AppStart/Twig.php';
require_once 'Controllers/BaseController.php';


class RouteHandler{

    public static $request;
    private static $baseUrl;
    private static $controllers;

    public static function Initialize()
    {
        self::$request = Request::createFromGlobals();
        self::$baseUrl = self::$request->getBasePath();
        self::$controllers = glob("Controllers/*.php");

    }

    /**
     * @param $action
     * @param $controller
     */
    public static function ExecuteAction($action, $controller)
    {
        // strict rendering addition
        $controllerToRequire = null;
        foreach(self::$controllers as $val){
            if(strcasecmp($val,'Controllers/' . $controller . 'Controller.php') == 0){
                $controllerToRequire = $val;
            }
        }

        if($controllerToRequire != null){
            // require the controller specified
            require_once($controllerToRequire);
            // way to call controllers dynamically and no need for switch case.
            $controllerString = $controller . 'Controller';
            $controller = new $controllerString;
            $controllerActions = get_class_methods($controller);

            // action to run inside controller
            $actionToRun = null;
            foreach($controllerActions as $val){
                if(strcasecmp($val,$action) == 0){
                    $actionToRun = $val;
                }
            }
            if($actionToRun != null){
                $reflection = new ReflectionClass($controllerString);
                $params = $reflection->getMethod($actionToRun)->getParameters();

                if(count($params) == 1){
                    if(count(self::$request->request) > 0 ){
                        $model = new stdClass();
                        foreach(self::$request->request  as $key => $value){
                            $model->$key = $value;
                        }
                        $controller->$actionToRun($model);
                    }
                    else{
                        $controller->$actionToRun(null);
                    }
                }
                else{
                    $controller->$actionToRun(null);
                }
                return;
            }

        }

         self::Error("The routing url is invalid!");

    }

    private static function Error($message){
        require_once("Controllers/ErrorController.php");
        $controller = new ErrorController();
        $controller->Index($message);
    }

    /**
     * @param $action - redirection action
     * @param $controller - redirection controller
     */
    public static function RedirectToAction($action, $controller)
    {
        $urlUtil = new UrlUtil();
        header('Location: ' . $urlUtil->Action($action,$controller));
    }

    private static function IsAjaxRequest()
    {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest');
    }

    public static function Start(){
        RouteHandler::Initialize();

        // default routing values
        $controller = 'Home';
        $action     = 'Index';

        if(self::$request->query->get("controller") != null){
            $controller = self::$request->query->get("controller");
        }
        if(self::$request->query->get("action") != null){
            $action = self::$request->query->get("action");
        }
        if(self::IsAjaxRequest()){
            return self::ExecuteAction($action,$controller);
        }

        TwigLayer::Start($action,$controller);
    }

}

?>