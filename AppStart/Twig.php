<?php

/**
 * Created by PhpStorm.
 * User: Treax
 * Date: 16.09.2016
 * Time: 18:40
 */


class TwigLayer
{

    private static $twig;

    public static function Initialize()
    {
        $loader = new Twig_Loader_Filesystem('Views/');
        self::$twig = new Twig_Environment($loader, array(
           // 'cache' => 'ViewsCache/', // cache - commented out to be disabled
        ));

        // add translate function to twig
        self::$twig->addFunction(new Twig_SimpleFunction("__",function($key,$isMD5 = false,$lang= null){
            return TranslationService::Translate($key,$isMD5,$lang);
        }));

        // add render partial function
        self::$twig->addFunction(new Twig_SimpleFunction("RenderPartial",function($action,$controller){
            return RouteHandler::ExecuteAction($action,$controller);
        }));

        // add function to check rights
        self::$twig->addFunction(new Twig_SimpleFunction("HasRights",function($right,$userId = null){
            return UserService::UserHasRights($right,$userId);
        }));

        // add translate function to twig
        self::$twig->addGlobal("URL",new UrlUtil());


    }

    public static function Render($template, $model)
    {
        $template = self::$twig->loadTemplate($template);
        echo $template->render($model);
    }

    public static function Start($action,$controller){
        self::Render("_Layout.tpl",array("action" => $action, "controller" => $controller));
    }
}

TwigLayer::Initialize();
